import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';

import { routes as loginRoutes } from './lobby/login/login.routing';
import { routes as registerRoutes } from './lobby/register/register.routing';

export const routes: Routes = [
  { path: '', redirectTo: 'admin', pathMatch: 'full' },
  { path: '**', redirectTo: 'admin/grid' },
];

routes.push(...loginRoutes);
routes.push(...registerRoutes);

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });
