export enum CRUD {
  Insert,
  Edit,
  Delete,
}

export enum ModalSizes {
  Small,
  Large,
}
