import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ToasterService } from 'angular2-toaster';

import { EntityModalComponent } from './entity-modal.component';
import { GenericDateModule } from '../../generic-date/generic-date.module';
import { MdControlsModule } from '../../material';

import { MainPipeModule } from '../../pipes/main-pipe.module';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    MainPipeModule,
    GenericDateModule,
    MdControlsModule,
  ],
  declarations: [
    EntityModalComponent,
  ],
  providers: [
    ToasterService,
  ],
  entryComponents: [
    EntityModalComponent,
  ],
})
export class EntityModalModule { }
