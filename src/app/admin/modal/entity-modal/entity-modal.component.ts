import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ToasterService } from 'angular2-toaster';


import { DataEntity, GridField, GridEditor } from '../../models';
import { CRUD } from '../';
import { RestService } from '../../services';

@Component({
  selector: 'nga-entity-modal',
  styleUrls: [('./entity-modal.scss')],
  templateUrl: './entity-modal.html',
})
export class EntityModalComponent implements OnInit {
  private _entity: DataEntity;
  get entity(): DataEntity { return this._entity; }
  set entity(value: DataEntity) {
    switch (this.crud) {
      case CRUD.Insert:
        break;
      case CRUD.Edit:
        value.fields = value.fields.filter(f => f.name !== 'IDProperty');
        break;
    }
    this._entity = value;
  }
  crud: CRUD;
  isCancellable: boolean;
  modalHeader: string;
  resource: string;
  service: RestService;
  closed: (result: DataEntity) => void;

  constructor(public dialogRef: MdDialogRef<EntityModalComponent>, private toasterService: ToasterService) {
    this.isCancellable = false;
  }

  ngOnInit() { }


  submit() {
    switch (this.crud) {
      case CRUD.Insert: this.service.insertResource(this.resource, this.entity.model).then(resp => {
        if (resp.ContainsItem === true) {
          const inserted = new DataEntity(this.entity.resource, this.entity.fields, resp.SingleResponse);
          this.dialogRef.afterClosed().subscribe(() => this.closed(inserted));
          this.dialogRef.close();
        } else {
          this.toasterService.pop('warning', 'Error', resp.Message);
        }
      });
        break;
      case CRUD.Edit: this.service.editResource(this.resource, this.entity.model).then(resp => {
        if (resp.ContainsItem === true) {
          const edited = new DataEntity(this.entity.resource, this.entity.fields, resp.SingleResponse);
          this.dialogRef.afterClosed().subscribe(() => this.closed(edited));
          this.dialogRef.close();
        } else {
          this.toasterService.pop('warning', 'Error', resp.Message);
        }
      });
        break;
    }
  }
  cancel() {
    this.dialogRef.close();
  }
}
