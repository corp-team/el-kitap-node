import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalComponent } from './modal.component';
import { EntityModalModule } from './entity-modal/entity-modal.module';
import { DefaultModalModule } from './default-modal/default-modal.module';
import { RestService } from '../services';

@NgModule({
  imports: [
    CommonModule,
    EntityModalModule,
    DefaultModalModule,
  ],
  declarations: [
    ModalComponent,
  ],
  providers: [
    RestService,
  ],
})
export class ModalModule { }
