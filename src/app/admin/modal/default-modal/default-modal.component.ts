import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'nga-modal',
  styleUrls: [('./default-modal.component.scss')],
  templateUrl: './default-modal.component.html',
})
export class DefaultModalComponent implements OnInit {
  isCancellable: boolean;
  modalHeader: string;
  modalContent: string;
  closed: (result: boolean) => void;

  constructor(public dialogRef: MdDialogRef<DefaultModalComponent>) {
    this.isCancellable = false;
  }

  ngOnInit() { }

  affirmative() {
    this.dialogRef.afterClosed().subscribe(() => this.closed(true));
    this.dialogRef.close();
  }
  cancel() {
    this.dialogRef.afterClosed().subscribe(() => this.closed(false));
    this.dialogRef.close();
  }
}
