import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { DefaultModalComponent } from './default-modal.component';
import { GenericDateModule } from '../../generic-date/generic-date.module';
import { MdControlsModule } from '../../material';

import { MainPipeModule } from '../../pipes/main-pipe.module';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    MainPipeModule,
    GenericDateModule,
    MdControlsModule,
  ],
  declarations: [
    DefaultModalComponent,
  ],
  providers: [
  ],
  entryComponents: [
    DefaultModalComponent,
  ],
})
export class DefaultModalModule { }
