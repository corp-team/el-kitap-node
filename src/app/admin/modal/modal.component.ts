import { Component } from '@angular/core';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';

import { DefaultModalComponent } from './default-modal/default-modal.component';
import { EntityModalComponent } from './entity-modal/entity-modal.component';
import { ModalSizes, CRUD } from './modal-enums';
import { DataEntity } from '../models';
import { RestService } from '../services';

@Component({
  selector: 'nga-modal',
  styleUrls: ['./modal.scss'],
  templateUrl: './modal.html',
})
export class ModalComponent {

  constructor(public dialog: MdDialog) { }

  entityForm(restService: RestService, data: DataEntity, header: string, forCRUD: CRUD): Promise<DataEntity> {
    const dialogRef = this.dialog.open(EntityModalComponent, { disableClose: true, width: '40%' });
    dialogRef.componentInstance.modalHeader = header;
    dialogRef.componentInstance.crud = forCRUD;
    dialogRef.componentInstance.entity = data;
    dialogRef.componentInstance.resource = data.resource;
    dialogRef.componentInstance.service = restService;
    return new Promise<DataEntity>((resolve, reject) => dialogRef.componentInstance.closed = resolve);
  }

  showStatic(content: string, header: string, size: ModalSizes) {
    const dialogRef = this.dialog.open(DefaultModalComponent, { disableClose: true });
    dialogRef.componentInstance.modalHeader = header;
    dialogRef.componentInstance.modalContent = content;
    return new Promise<any>((resolve, reject) =>
      dialogRef.afterClosed().subscribe(resolve));
  }

  show(content: string, header: string, size: ModalSizes, config?: MdDialogConfig) {
    const dialogRef = this.dialog.open(DefaultModalComponent, config);
    dialogRef.componentInstance.modalHeader = header;
    dialogRef.componentInstance.modalContent = content;
    return new Promise<any>((resolve, reject) =>
      dialogRef.afterClosed().subscribe(resolve));
  }

  showConfirm(content: string, header: string): Promise<boolean> {
    const dialogRef = this.dialog.open(DefaultModalComponent, { disableClose: true, width: '40%' });
    dialogRef.componentInstance.modalHeader = header;
    dialogRef.componentInstance.modalContent = content;
    dialogRef.componentInstance.isCancellable = true;
    return new Promise<boolean>((resolve, reject) => dialogRef.componentInstance.closed = resolve);
  }

}
