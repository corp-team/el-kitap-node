import { Component, AfterViewInit, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { ModalComponent, ModalSizes, CRUD } from '../modal';

import { GridField, DataEntity } from '../models';
import { RestService } from '../services/rest.service';

@Component({
  selector: 'nga-generic-grid',
  templateUrl: './generic-grid.html',
  styleUrls: ['./generic-grid.scss'],
})
export class GenericGridComponent implements AfterViewInit, OnInit, OnDestroy {

  title: string;
  collection: DataEntity[] = new Array<DataEntity>();
  private resource: string;
  private sub: Subscription;
  private fields: GridField[] = new Array<GridField>();

  constructor(private route: ActivatedRoute, private service: RestService, private modal: ModalComponent) {
  }

  ngOnInit() {
    this.sub = this.route
      .data
      .subscribe(v => {
        this.resource = v['resource'];
        this.title = `${this.resource[0].toLocaleUpperCase()}${this.resource.slice(1)} Table`;
      });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
  ngAfterViewInit() {
    this.loadGrid();
  }
  onInsert(): void {
    const insertFields = new Array<GridField>();
    for (const field of this.fields) {
      if (field.name !== 'IDProperty') {
        insertFields.push(field);
      }
    }
    const data = new DataEntity(this.resource, insertFields);
    this.modal.entityForm(this.service, data,
      `Insert New ${this.resource[0].toLocaleUpperCase()}${this.resource.slice(1)}`,
      CRUD.Insert)
      .then((e) => this.reloadGrid(e, CRUD.Insert));
  }
  onEdit(entity): void {
    const data = new DataEntity(this.resource, this.fields, entity);
    this.modal.entityForm(this.service, data,
      `Edit Existing ${this.resource[0].toLocaleUpperCase()}${this.resource.slice(1)}`,
      CRUD.Edit)
      .then((e) => this.reloadGrid(e, CRUD.Edit));
  }
  onDelete(entity): void {
    this.modal.showConfirm('Will be Deleted', 'Confirm').then((affirmative) => {
      if (affirmative) {
        this.service.deleteResource(this.resource, entity.IDProperty).then((e) =>
          this.reloadGrid(new DataEntity(this.resource, this.fields, e.SingleResponse), CRUD.Delete));
      }
    });
  }
  onRelaod() {
    this.loadGrid();
  }
  private loadGrid(order: string = 'ASC'): Promise<any> {
    return this.service.getFields(this.resource).then(fields => {
      this.setSettings(fields);
      return this.service.getResource(this.resource,
        { field: 'IDProperty', order },
        { begin: 0, end: -1 }).then(response => {
          const collection = response.CollectionResponse;
          this.collection = collection.map(c => new DataEntity(this.resource, this.fields, c));
        });
    });
  }
  private reloadGrid(modified: DataEntity, crud: CRUD) {
    switch (crud) {
      case CRUD.Delete:
        this.collection = this.collection.filter(c => c.model.IDProperty !== modified.model.IDProperty);
        break;
      case CRUD.Edit:
        for (const entity of this.collection) {
          if (entity.model.IDProperty === modified.model.IDProperty) {
            entity.model = modified.model;
          }
        }
        break;
      case CRUD.Insert:
        this.loadGrid('DESC');
        break;
    }

  }
  private setSettings(fields: any) {
    this.fields = new Array<GridField>();
    for (const prop in fields) {
      if (fields.hasOwnProperty(prop)) {
        const field = fields[prop];
        this.fields.push(field);
      }
    }
  }
}
