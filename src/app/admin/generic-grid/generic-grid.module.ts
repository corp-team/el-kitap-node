import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgaModule } from '../../theme/nga.module';

import { routing } from './generic-grid.routing';

import { ModalComponent } from '../modal';
import { ModalModule } from '../modal/modal.module';
import { MdControlsModule } from '../material';
import { RestService } from '../services/rest.service';
import { MainPipeModule } from '../pipes/main-pipe.module';

import { GenericGridComponent } from './generic-grid.component';

@NgModule({
  imports: [
    CommonModule,
    NgaModule,
    routing,
    MdControlsModule,
    ModalModule,
    MainPipeModule,
  ],
  declarations: [
    GenericGridComponent,
  ],
  providers: [
    RestService,
    ModalComponent,
  ],
})
export class GenericGridModule { }
