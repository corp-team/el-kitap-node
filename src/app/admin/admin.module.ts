import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { routing } from './admin.routing';
import { NgaModule } from '../theme/nga.module';
import { AppTranslationModule } from '../app.translation.module';

import { AdminComponent } from './admin.component';
import { RestService } from './services';
import { GenericGridModule } from './generic-grid/generic-grid.module';
import { ModalModule } from './modal/modal.module';
import { MdControlsModule } from './material';

import { routing as loginRouting } from '../lobby/login/login.routing';
import { routing as registerRouting } from '../lobby/register/register.routing';

@NgModule({
  imports: [
    CommonModule,
    AppTranslationModule,
    NgaModule,
    routing,
    loginRouting,
    registerRouting,
    GenericGridModule,
    MdControlsModule,
  ],
  declarations: [
    AdminComponent,
  ],
  providers: [
    RestService,
  ],
})
export class AdminModule {
}
