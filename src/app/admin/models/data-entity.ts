import { GridField } from './grid-field';

export class DataEntity {
  fields: GridField[];
  resource: string;
  model: any;
  constructor(resource: string, fields: GridField[], entity: any = null) {
    this.resource = resource;
    this.fields = fields;
    this.model = entity || {};
  }
}
