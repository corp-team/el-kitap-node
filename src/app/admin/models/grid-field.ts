export class GridEditor {
  type: string;
  component: any;
  config: {
    list: [any],
  };
}

export class GridField {
  placeholder: string;
  required: boolean;
  title: string;
  name: string;
  inputType: string;
  type: string;
  editor: GridEditor;
  constructor(placeholder: string, required: boolean, title: string, type: string, editor: GridEditor) {
    this.placeholder = placeholder;
    this.required = required;
    this.title = title;
    this.type = type;
    this.editor = editor;
  }
}
