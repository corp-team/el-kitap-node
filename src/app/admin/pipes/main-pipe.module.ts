import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InputTypeFilter } from './input-type.pipe';

@NgModule({
  declarations: [InputTypeFilter],
  imports: [CommonModule],
  exports: [InputTypeFilter]
})

export class MainPipeModule { }
