import { Injectable, Pipe, PipeTransform } from '@angular/core';
import { GridField } from '../models';

@Pipe({
  name: 'inputTypeFilter',
})
@Injectable()
export class InputTypeFilter implements PipeTransform {
  transform(fields: GridField[], types: string[]): any {
    const filtered = fields.filter(field => {
      let filters = false;
      for (const type of types) {
        if (field.editor.type === type) {
          filters = true;
        }
      }
      return filters;
    });
    return filtered;
  }
}
