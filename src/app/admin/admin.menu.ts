export const ADMIN_MENU = [
  {
    path: 'admin',
    children: [
      {
        path: 'azalar',
        data: {
          menu: {
            title: 'general.menu.azalar',
            icon: 'ion-android-person',
            selected: false,
            expanded: false,
            order: 1,
          },
        },
      },
      {
        path: 'kutuphane',
        data: {
          menu: {
            title: 'general.menu.kutuphane',
            icon: 'ion-university',
            selected: false,
            expanded: false,
            order: 2,
          },
        },
      },
    ],
  },
];
