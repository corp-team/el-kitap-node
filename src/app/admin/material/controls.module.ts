import { NgModule } from '@angular/core';

import { ToasterModule } from 'angular2-toaster';

import { DataTableModule } from 'angular2-datatable';

import {
  MdButtonModule,
  MdCheckboxModule,
  MdInputModule,
  MdSelectModule,
  MdOptionModule,
  MdDatepickerModule,
  MdNativeDateModule,
  MdCardModule,
  MdDialogModule,
  MdListModule,
  MdIconModule,
} from '@angular/material';

const MODULES = [
  ToasterModule,
  DataTableModule,
  MdButtonModule,
  MdCheckboxModule,
  MdInputModule,
  MdSelectModule,
  MdOptionModule,
  MdDatepickerModule,
  MdNativeDateModule,
  MdCardModule,
  MdDialogModule,
  MdListModule,
  MdIconModule,
];

@NgModule({
  imports: MODULES,
  exports: MODULES,
})
export class MdControlsModule { }
