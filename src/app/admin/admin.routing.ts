import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { ModuleWithProviders } from '@angular/core';
// noinspection TypeScriptValidateTypes

// export function loadChildren(path) { return System.import(path); };
import { GenericGridComponent } from './generic-grid/generic-grid.component';

export const routes: Routes = [
  {
    path: 'admin',
    component: AdminComponent,
    children: [
      { path: '', redirectTo: 'azalar', pathMatch: 'full' },
      { path: 'azalar', component: GenericGridComponent, data: { resource: 'aza' } },
      { path: 'kutuphane', component: GenericGridComponent, data: { resource: 'kutuphane' } },
    ],
  },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
