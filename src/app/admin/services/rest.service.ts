import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent, ModalSizes } from '../modal';
import { GlobalState } from '../../global.state';
import { Session, Aza } from '../../lobby/models';

import { GridField } from '../models';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/retryWhen';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Injectable()
export class RestService {

  private api: string;

  constructor(private http: Http, private router: Router, private _state: GlobalState, private modal: ModalComponent) {
    this.api = 'http://192.168.1.34:114/api/v1';
  }

  tokenize(session: Session = null): Promise<Session> {
    session = session === null ? this._state.fetch<Session>('session') : session;
    const url = `${this.api}/aza/jeton`;
    return new Promise<Session>((resolve, reject) => {
      if (!session) {
        this.router.navigate(['/oturum-ac'], { replaceUrl: true });
      } else {
        this.http.post(url, session)
          .map(response => {
            if (response.status === 200) {
              return response.json();
            } else {
              this.modal.show('Invalid Response', 'Error Occurred', ModalSizes.Small);
              return { error: new Error(response.status.toString()) };
            }
          })
          .subscribe((data) => {
            if (!data.error) {
              this._state.upsert('session', data);
              resolve(data);
            }
          }, (error) => {
            this.modal.show(error.ClientMessage, 'Error Occurred', ModalSizes.Large);
            reject(error);
          });
      }
    });
  }

  get(route): Promise<any> {
    const url = `${this.api}/${route}`;
    return new Promise<any>((resolve, reject) => {
      this.tokenize().then(s => {
        const headers = new Headers();
        headers.append('X-HDS-Tahdit', s.Token.AccessToken);
        this.http.get(url, { headers })
          .timeout(7000)
          .retryWhen((errors) => errors.delay(9000))
          .map(response => response.json())
          .subscribe((result) => {
            console.info('get', result);
            resolve(result);
          }, (error) => {
            this.modal.show(error.message, 'Error Occurred', ModalSizes.Large);
            reject(error);
          });
      });
    });
  }
  post(route: string, entity: any): Promise<any> {
    const url = `${this.api}/${route}`;
    return new Promise<any>((resolve, reject) => {
      this.tokenize().then(s => {
        const headers = new Headers();
        headers.append('X-HDS-Tahdit', s.Token.AccessToken);
        this.http.post(url, entity)
          .map(response => response.json())
          .subscribe((result) => {
            console.info('post', result);
            resolve(result);
          }, (error) => {
            this.modal.showStatic(error.Message, 'Post Error Occurred', ModalSizes.Large)
              .then((val) => reject(error));
          });
      });
    });
  }

  put(route: string, entity: any): Promise<any> {
    const url = `${this.api}/${route}`;
    return new Promise<any>((resolve, reject) => {
      this.tokenize().then(s => {
        const headers = new Headers();
        headers.append('X-HDS-Tahdit', s.Token.AccessToken);
        this.http.put(url, entity)
          .map(response => response.json())
          .subscribe((result) => {
            console.info('put', result);
            resolve(result);
          }, (error) => {
            this.modal.showStatic(error.Message, 'Put Error Occurred', ModalSizes.Large)
              .then((val) => reject(error));
          });
      });
    });
  }
  delete(route: string): Promise<any> {
    const url = `${this.api}/${route}`;
    return new Promise<any>((resolve, reject) => {
      this.tokenize().then(s => {
        const headers = new Headers();
        headers.append('X-HDS-Tahdit', s.Token.AccessToken);
        this.http.delete(url)
          .map(response => response.json())
          .subscribe((result) => {
            console.info('deleted', result);
            resolve(result);
          }, (error) => {
            this.modal.show(error.Message, 'Delete Error Occurred', ModalSizes.Large);
            reject(error);
          });
      });
    });
  }

  insertResource(resource: string, entity: any): Promise<any> {
    const route = `${resource}/insert`;
    return this.post(route, entity);
  }
  editResource(resource: string, entity: any): Promise<any> {
    const route = `${resource}/edit`;
    return this.put(route, entity);
  }
  deleteResource(resource: string, id: number): Promise<any> {
    const route = `${resource}/delete/${id}`;
    return this.delete(route);
  }
  getResource(resource: string,
    sort: { field: string, order: string },
    range: { begin: number, end: number },
    filter: { field: string, contains: string } = null): Promise<any> {
    const s = `sort=['${sort.field}','${sort.order}']`;
    const r = `&range=[${range.begin}, ${range.end}]`;
    const f = filter !== null ? `&filter={${filter.field}:'${filter.contains}'}` : '';
    const route = `${resource}/all?${s}${r}${f}`;
    return this.get(route);
  }
  getFields(resource: string): Promise<any> {
    const route = `${resource}/grid-fields`;
    return this.get(route);
  }

}
