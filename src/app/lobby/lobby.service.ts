import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent, ModalSizes } from '../admin/modal';
import { Session, Aza } from './models';
import { GlobalState } from '../global.state';
import { RestService } from '../admin/services';

import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/retryWhen';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';

@Injectable()
export class LobbyService {

  constructor(private http: RestService, private modal: ModalComponent, private _state: GlobalState) {
  }

  login(session: Session): Promise<any> {
    const route = `aza/oturum-ac`;
    return new Promise<any>((resolve, reject) => {
      this.http.tokenize(session).then(s => {
        this.http.post(route, s).then(data => {
          this._state.upsert('session', data);
          resolve(data);
        });
      });
    });
  }

  register(aza: Aza): Promise<any> {
    const route = `aza/insert`;
    return new Promise<any>((resolve, reject) => {
      this.http.tokenize().then(s => {
        this.http.post(route, aza).then(data => {
          this._state.upsert('new-aza', data);
          resolve(data);
        });
      });
    });
  }

}
