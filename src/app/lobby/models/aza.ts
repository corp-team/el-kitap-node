export class Aza {
  AzaTCKN: string;
  AzaSifre: string;
  KunyeIsim: string;
  KunyeSoyisim: string;

  constructor(tckn: string, sifre: string, isim: string, soyisim: string) {
    this.AzaTCKN = tckn;
    this.AzaSifre = sifre;
    this.KunyeIsim = isim;
    this.KunyeSoyisim = soyisim;
  }

}
