export class Session {
  UserName: string;
  Password: string;
  Token: {
    AccessToken: string,
    Success: boolean,
  };

  constructor(uname: string, pwd: string) {
    this.UserName = uname;
    this.Password = pwd;
  }

}
