import { Component } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { LobbyService } from '../lobby.service';
import { Session } from '../models';

import { ModalComponent, ModalSizes } from '../../admin/modal';

@Component({
  selector: 'nga-login',
  templateUrl: './login.html',
  styleUrls: ['./login.scss'],
})
export class LoginComponent {

  form: FormGroup;
  email: AbstractControl;
  password: AbstractControl;
  submitted: boolean = false;

  constructor(fb: FormBuilder, private router: Router, private modal: ModalComponent, private http: LobbyService) {
    this.form = fb.group({
      'email': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
    });

    this.email = this.form.controls['email'];
    this.password = this.form.controls['password'];
  }

  onSubmit(values: Object): void {
    this.submitted = true;
    if (this.form.valid) {
      const session = new Session(values['email'], values['password']);
      this.http.login(session).then(resp => {
        if (resp.Token.Success === true) {
          this.modal.showStatic(resp.ClientMessage, 'Login Successful', ModalSizes.Large)
            .then((value) => this.router.navigate(['admin/azalar']));
        } else {
          this.modal.show(resp.ClientMessage, 'Login Failed', ModalSizes.Large);
        }
      });
    }
  }
}
