import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgaModule } from '../theme/nga.module';
import { AppTranslationModule } from '../app.translation.module';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

import { routing as loginRouting } from './login/login.routing';
import { routing as registerRouting } from './register/register.routing';

import { LobbyService } from './lobby.service';
import { RestService } from '../admin/services';

@NgModule({
  imports: [
    CommonModule,
    loginRouting,
    registerRouting,
    AppTranslationModule,
    ReactiveFormsModule,
    FormsModule,
    NgaModule,
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
  ],
  providers: [
    RestService,
    LobbyService,
  ],
})
export class LobbyModule { }
