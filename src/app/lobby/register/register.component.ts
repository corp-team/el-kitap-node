import { Component } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { EmailValidator, EqualPasswordsValidator } from '../../theme/validators';

import { LobbyService } from '../lobby.service';
import { Session, Aza } from '../models';


import { ModalComponent, ModalSizes } from '../../admin/modal';

@Component({
  selector: 'nga-register',
  templateUrl: './register.html',
  styleUrls: ['./register.scss'],
})
export class RegisterComponent {

  form: FormGroup;
  fname: AbstractControl;
  lname: AbstractControl;
  email: AbstractControl;
  password: AbstractControl;
  repeatPassword: AbstractControl;
  passwords: FormGroup;

  submitted: boolean = false;

  constructor(fb: FormBuilder, private router: Router, private modal: ModalComponent, private ls: LobbyService) {

    this.form = fb.group({
      'fname': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'lname': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      'email': ['', Validators.compose([Validators.required, Validators.minLength(11)])],
      'passwords': fb.group({
        'password': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
        'repeatPassword': ['', Validators.compose([Validators.required, Validators.minLength(4)])],
      },
        {
          validator: EqualPasswordsValidator.validate('password', 'repeatPassword'),
        }),
    });

    this.fname = this.form.controls['fname'];
    this.lname = this.form.controls['lname'];
    this.email = this.form.controls['email'];
    this.passwords = <FormGroup>this.form.controls['passwords'];
    this.password = this.passwords.controls['password'];
    this.repeatPassword = this.passwords.controls['repeatPassword'];
  }

  onSubmit(values: Object): void {
    this.submitted = true;
    if (this.form.valid) {
      const aza = new Aza(values['email'], values['passwords']['password'], values['fname'], values['lname']);
      this.ls.register(aza).then(resp => {
        if (resp.ContainsItem === true) {
          this.modal.show(
            `You're Welcome ${resp.SingleResponse.KunyeIsim} ${resp.SingleResponse.KunyeSoyisim}!`,
            'Register Successful',
            ModalSizes.Large).then((value) => this.router.navigate(['oturum-ac']));
        } else {
          this.modal.show(resp.Message, 'Register Failed', ModalSizes.Large);
        }
      });
    }
  }
}
