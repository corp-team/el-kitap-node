import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './register.component';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: 'kayit-ol',
    component: RegisterComponent,
  },
];

export const routing = RouterModule.forChild(routes);
