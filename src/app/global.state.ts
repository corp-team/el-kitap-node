import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Cookie } from 'ng2-cookies';

@Injectable()
export class GlobalState {

  private _data = new Subject<Object>();
  private _dataStream$ = this._data.asObservable();

  private _subscriptions: Map<string, Function[]> = new Map<string, Function[]>();

  constructor() {
    this._dataStream$.subscribe((data) => this._onEvent(data));
  }

  notifyDataChanged(e, value) {

    const current = this._data[e];
    if (current !== value) {
      this._data[e] = value;

      this._data.next({
        event: e,
        data: this._data[e],
      });
    }
  }

  upsert(key: string, value: any): GlobalState {
    if (!Cookie.check(key)) {
      const d = new Date();
      d.setMinutes(d.getMinutes() + 30);
      Cookie.set(key, JSON.stringify(value), d);
    } else {
      Cookie.set(key, JSON.stringify(value));
    }
    this._data[key] = value;
    return this;
  }
  fetch<T>(key: string): T {
    return this._data[key] || (Cookie.check(key) ? JSON.parse(Cookie.get(key)) : null);
  }

  subscribe(event: string, callback: Function) {
    const subscribers = this._subscriptions.get(event) || [];
    subscribers.push(callback);

    this._subscriptions.set(event, subscribers);
  }

  _onEvent(data: any) {
    const subscribers = this._subscriptions.get(data['event']) || [];

    subscribers.forEach((callback) => {
      callback.call(null, data['data']);
    });
  }
}
